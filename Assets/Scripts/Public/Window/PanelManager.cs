﻿using UnityEngine;
using System.Collections;
using System;
[Serializable]
public class PanelManager {

    public int windowsSort = 20;
    public PanelInfo[] panels;
    public delegate void DelegatePanelChanged(string s);
    public event DelegatePanelChanged OnPanelChangeEvent;

    public void SetSort(int sort)
    {
        windowsSort = sort;
    }

    public int FindPanel(string tag)
    {
        return Array.FindIndex(panels, p => p.tag.Contains(tag));
    }

    public void OpenPanel(string tag)
    {
        WindowsManager.TurnOffAllActiveWindows(windowsSort);
        panels[FindPanel(tag)].panel.Active();
        if (OnPanelChangeEvent != null)
            OnPanelChangeEvent(tag);
    }

    public void DeactiveAll()
    {
        WindowsManager.TurnOffAllActiveWindows(windowsSort);
    }
}
[Serializable]
public class PanelInfo
{
    public string tag;
    public Window panel;
}
