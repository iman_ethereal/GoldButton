﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class WindowsManager : MonoBehaviour {

    public const int MAINPANEL_SORT_NUMBER = 2;
    public const int HEADERPANEL_SORT_NUMBER = 1;

    public static List<Window> activeWindows = new List<Window>();

    public static void AddWindow(Window w)
    {
        if (!activeWindows.Contains(w))
            activeWindows.Add(w);
    }

    public static void RemoveWindow(Window w)
    {
        if (activeWindows.Contains(w))
            activeWindows.Remove(w);
    }

    public static void TurnOffAllActiveWindows(int sort)
    {
        for (int cnt = 0; cnt < activeWindows.Count; cnt++)
        {
            if(activeWindows[cnt].sort == sort)
                activeWindows[cnt].Deactive();
        }
    }
}