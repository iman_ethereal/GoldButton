﻿using UnityEngine;
using System.Collections;

public class Window : MonoBehaviour {
        
    public GameObject my;
    public int sort; 

    public bool isActive
    {
        get { return my.activeSelf; }
    }

    public void Active()
    {
        my.SetActive(true);
        WindowsManager.AddWindow(this);
    }

    public void Deactive()
    {
        my.SetActive(false);
        WindowsManager.RemoveWindow(this); 
        
    }
}
