﻿using UnityEngine;

/// <summary>
/// Singleton pattern implementation with C# but in unity. Considering unity's special way of 
/// handling objects, this class provides a common singleton functionality.Although several
/// problems are handled, there are some major notes to be read before usage.
/// The singleton's Instance property should not be called from code within constructor of 
/// any class deriving from MonoBehavior.This is due to unity3d's limitation. Failure 
/// conforming to this "law" will result in unity's complains like this:
/// 
/// BlahBlahMethod is not allowed to be called from a MonoBehaviour
/// constructor(or instance field initializer), call it in Awake or Start
/// instead.Called from MonoBehaviour 'BlahBlah' on game object 'Blah'.
/// See "Script Serialization" page in the Unity Manual for further details.
/// </summary>
/// <typeparam name="T"></typeparam>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    static private object mInstanceAccessLock = new object();
    static private T mInstance;

    /// <summary>
    /// Finds and returns the only instance for the class. Will create one if necessary.
    /// </summary>
    static public T Instance
    {
        get
        {
            if (mInstance == null)
                lock (mInstanceAccessLock)
                {
                    if (mInstance == null)
                    {
                        Object[] instances = Object.FindObjectsOfType(typeof(T));

                        // There is an instance in scene. Use that.
                        if (instances.Length == 1)
                            mInstance = (T)(instances[0]);

                        // There is no instance in scene. Create one.
                        else if (instances.Length == 0)
                            mInstance = SingletonExtensions.SingletonContainer.AddComponent<T>();

                        // There are multiple instances in scene. Use the last one.
                        else
                        {
                            Debug.LogError(string.Format("Multiple instances of {0} in scene. Picking the last one.", typeof(T).Name));
                            mInstance = (T)instances[instances.Length - 1];
                        }
                    }
                }

            return mInstance;
        }
    }
}

static class SingletonExtensions
{
    static private object mSingletonContainerAccessLock = new object();
    static private GameObject mSingletonContainer;

    static internal GameObject SingletonContainer
    {
        get
        {
            lock (mSingletonContainerAccessLock)
            {
                if (mSingletonContainer == null)
                {
                    mSingletonContainer = new GameObject("Singleton Container");
                    mSingletonContainer.hideFlags = HideFlags.HideInHierarchy;
                    Object.DontDestroyOnLoad(mSingletonContainer);
                }
            }

            return mSingletonContainer;
        }
    }
}
