﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ShopInfoContent : MonoBehaviour {

    public Text nameText;
    public Image iconSlot;

    public bool IsActive
    {
        get { return gameObject.activeSelf; }
    }
    public void Active(ShopInfo info)
    {
        gameObject.SetActive(true);
        nameText.text = info.name;
        iconSlot.sprite = info.icon;
    }

    public void Deactive()
    {
        gameObject.SetActive(false);
    }
}
