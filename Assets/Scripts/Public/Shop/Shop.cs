﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class Shop{

    public ShopBaseInfo[] infos;
    public ShopBaseInfo[] defaults;

    public delegate void DelegateShop(Shop shop, string tag, int id);
    public event DelegateShop OnBuyEvent, OnSelectEvent, OnDeselect, OnUsed;

    public void Buy(string tag, int id)
    {
        FindItem(tag).Buy(id);
        if (OnBuyEvent != null)
            OnBuyEvent(this, tag, id);
    }

    public void Use(string tag, int id)
    {
        ShopInfo info = FindItem(tag, id);
        info.Decrease();
        if (OnUsed != null)
            OnUsed(this, tag, id);
    }

    public void Select(string tag, int id)
    {
        FindItem(tag).DeselectAll();
        FindItem(tag).Select(id);
        if (OnSelectEvent != null)
            OnSelectEvent(this, tag, id);
    }

    public void SelectDefaults( string tag, int id )
    {
        FindItem(tag).DeselectAll();
        FindDefaultItem(tag).FindItem(id).Select();
    }

    public void Deselect(string tag, int id)
    {
        FindItem(tag).Deselect(id);
        if (OnDeselect != null)
            OnDeselect(this, tag, id);
    }

    public bool Contains(string tag)
    {
        return FindItem(tag) != null;
    }
    public bool ContainsDefault(string tag)
    {
        return FindDefaultItem(tag) != null;
    }

    public ShopBaseInfo FindItem( string tag)
    {
        return Array.Find(infos, s => s.tag.Contains(tag));
    }
    public ShopBaseInfo FindDefaultItem(string tag)
    {
        return Array.Find(defaults, s => s.tag.Contains(tag));
    }

    public ShopInfo FindItem(string tag, int id)
    {
        return FindItem(tag).FindItem(id);
    }

    public ShopInfo FindDefaultItem(string tag, int id)
    {
        if( ContainsDefault(tag))
        {
            return FindDefaultItem(tag).FindItem(id);
        }
        else
            return null;
    }

    public bool HaveItem(string tag)
    {
        return FindItem(tag).HasBuyedItem();
    }
    public bool HaveDefaultItem(string tag)
    {
        return FindDefaultItem(tag).HasBuyedItem();
    }

    public bool HaveSelected(string tag)
    {
        return FindItem(tag).HasSelectedItem();
    }
    public bool HaveDefaultSelected(string tag)
    {
        return FindDefaultItem(tag).HasSelectedItem();
    }

    public ShopInfo GetSelectedItem(string tag)
    {
        return FindItem(tag).GetSelectedItem();
    }

    public void SetDefault(string tag)
    {
        FindItem(tag).DeselectAll();

        if( ContainsDefault(tag) )
            FindDefaultItem(tag).Select(0);
    }

    public string GetSaveInformation()
    {
        string temp = "";
        for (int cnt = 0; cnt < infos.Length; cnt++)
        {
            string s = Statices.Separator(Statices.SEPARATOR_BETWEEN_ITEMS, infos[cnt].tag, infos[cnt].GetInformation());
            temp += Statices.Separator(Statices.SEPARATOR_BETWEEN_GROUPS, s);
        }
        temp = temp.TrimEnd(new char[] { Statices.SEPARATOR_BETWEEN_GROUPS });
        return temp;
    }

    public void SetLoadInformation(string txt)
    {
        string[] sp = txt.Split(new char[] { Statices.SEPARATOR_BETWEEN_GROUPS });
        for (int cnt = 0; cnt < sp.Length; cnt++)
        {
            string[] sp1 = sp[cnt].Split(new char[] { Statices.SEPARATOR_BETWEEN_ITEMS });
            FindItem(sp1[0]).SetInformation(sp1[1]);
        }
    }


}
[Serializable]
public class ShopBaseInfo
{
    public string tag;
    public ShopInfo[] items;

    public bool HasBuyedItem()
    {
        bool have = false;
        for (int cnt = 0; cnt < items.Length; cnt++)
        {
            if (items[cnt].buyed)
                have = true;
        }
        return have;
    }
    public bool HasSelectedItem()
    {
        bool have = false;
        for (int cnt = 0; cnt < items.Length; cnt++)
        {
            if (items[cnt].select)
                have = true;
        }
        return have;
    }

    public ShopInfo GetSelectedItem()
    {
        return Array.Find(items, s => s.select);
    }

    public void Buy( int id)
    {
        FindItem(id).Buy();
    }

    public void Select(int id)
    {
        FindItem(id).Select();
    }

    public void Deselect(int id)
    {
        FindItem(id).Deselect();
    }

    public void DeselectAll()
    {
        foreach (ShopInfo item in items)
        {
            item.Deselect();
        }
    }

    public ShopInfo FindItem(int id)
    {
        return Array.Find(items, h => h.id == id);
    }

    public string GetInformation()
    {
        string temp = "";
        string s = "";
        for (int cnt = 0; cnt < items.Length; cnt++)
        {
            if (items[cnt].buyed)
            {
                if (!items[cnt].select)
                    s = Statices.Separator(Statices.SEPARATOR_BETWEEN_ITEMSOFLIST, items[cnt].id, items[cnt].count);
                else
                    s = Statices.Separator(Statices.SEPARATOR_BETWEEN_ITEMSOFLIST, items[cnt].id, items[cnt].count, "@");

                s = s.TrimEnd(new char[] { Statices.SEPARATOR_BETWEEN_ITEMSOFLIST });
                temp += Statices.Separator(Statices.SEPARATOR_BETWEEN_LISTITEMS, s);
            }
        }
        temp = temp.TrimEnd(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        return temp;
    }
    public void SetInformation(string txt)
    {
        string[] sp = txt.Split(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        for (int cnt = 0; cnt < sp.Length; cnt++)
        {
            string[] sp1 = sp[cnt].Split(new char[] { Statices.SEPARATOR_BETWEEN_ITEMSOFLIST });
            if (!string.IsNullOrEmpty(sp[cnt]))
            {
                if (!sp[cnt].Contains("@"))
                {
                    FindItem(sp1[0].ToInt()).Buy();
                    FindItem(sp1[0].ToInt()).count = sp1[1].ToInt();
                }
                else
                {
                    sp[cnt] = sp[cnt].Trim(new char[] { '@' });
                    FindItem(sp1[0].ToInt()).Buy();
                    FindItem(sp1[0].ToInt()).Select();
                    FindItem(sp1[0].ToInt()).count = sp1[1].ToInt();
                }
            }
        }
    }
}

[Serializable]
public class ShopInfo
{
    public int id;
    public string name;
    public Nutrition nutrition;
    public Sprite icon;
    public bool select;
    public int count;

    public bool buyed
    {
        get { return count > 0; }
    }

    public void Buy()
    {
        count ++;
    }

    public void Decrease()
    {
        if(count > 0)
            count--;
    }

    public void Select()
    {
        select = true;
    }
    public void Deselect()
    {
        select = false;
    }
}
