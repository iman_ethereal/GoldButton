﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DoubleSlider : MonoBehaviour {

    public Image primary, secondry;
    public Animator animator;

    private float freezTime = .3f;
    private float lastClick;


    public void SetPrimary( Sprite icon )
    {
        primary.sprite = icon;
    }
    public void SetSecondary(Sprite icon)
    {
        secondry.sprite = icon;
    }

    public void SetImages( Sprite p, Sprite s )
    {
        primary.sprite = p;
        secondry.sprite = s;
    }

    public void GotoLeft(Action method)
    {
        if (lastClick < Time.time)
        {
            animator.CrossFadeInFixedTime("SliderToLeft", 0);
            lastClick = Time.time + freezTime;
            if (method != null)
                method();
        }
    }

    public void GotoRight( Action method )
    {
        if (lastClick < Time.time)
        {
            animator.CrossFadeInFixedTime("SliderToRight", 0);
            lastClick = Time.time + freezTime;
            if (method != null)
                method();
        }
    }
}
