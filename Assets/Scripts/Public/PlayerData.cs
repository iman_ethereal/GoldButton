﻿using UnityEngine;
using System.Collections;
using System;
public class PlayerData : Singleton<PlayerData> {

    public delegate void DelegateOnInfoChanged(int i);
    public event DelegateOnInfoChanged OnGoldChanged;
    public event DelegateOnInfoChanged OnStarChanged;
    public event DelegateOnInfoChanged OnEnergyChanged;

    public PlayerInfos info;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void BuyWithgold(int gold, Action doneMethod, Action failMethod)
    {
        if (HaveGold(gold))
        {
            DeceaseGold( gold);
            if (doneMethod != null)
                doneMethod();
        }
        else
        {
            if (failMethod != null)
                failMethod();
        }
    }

    public bool HaveGold(int gold)
    {
        return (gold <= info.gold && info.gold - gold >= 0);
    }

    public void DeceaseGold(int gold)
    {
        info.gold -= gold;
        if (OnGoldChanged != null)
            OnGoldChanged(info.gold);
    }
    public void IncreaseGold(int gold)
    {
        info.gold += gold;
        if (OnGoldChanged != null)
            OnGoldChanged(info.gold);
    }

    public void IncreaseStar(int i)
    {
        info.star += i;
        if (OnStarChanged != null)
            OnStarChanged(info.star);
    }

    public void DecreaseStar(int i)
    {
        info.star -= i;
        if (OnStarChanged != null)
            OnStarChanged(info.star);
    }
}

[Serializable]
public class PlayerInfos
{
    public int gold;
    public int star;

    public string GetSaveInformation()
    {
        return string.Concat(Statices.Separator(Statices.SEPARATOR_BETWEEN_LISTITEMS ,gold, star));
    }

    public void SetLoadInformation(string txt)
    {
        string[] sp = txt.Split(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        gold = sp[0].ToInt();
        star = sp[1].ToInt();
    }
}
