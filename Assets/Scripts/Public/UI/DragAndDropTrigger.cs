﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class DragAndDropTrigger : MonoBehaviour
{
    public ButtonTrigger trigger;
    public Image imageIcon;

    public delegate void DelegateDDT();
    public event DelegateDDT OnDragBeginEvent;
    public event DelegateDDT OnDragEndEvent;

    private object carriedObject = null;
    public void SetObject( object obj )
    {
        carriedObject = obj;
    }

    void Start()
    {
        trigger.OnDragPos = OnDrag;
        trigger.OnBeginDragEvent = OnBeginDrag;
        trigger.OnEndDragEvent = OnEndDrag;
    }

    public void OnBeginDrag(ButtonTrigger bt)
    {
        if (OnDragBeginEvent != null)
            OnDragBeginEvent();

        DragImageScript.Instance.Active(imageIcon.sprite, imageIcon.rectTransform.anchoredPosition, imageIcon.rectTransform.sizeDelta, carriedObject);
    }

    public void OnDrag( Vector2 pos )
    {
        DragImageScript.Instance.SetPosition(pos);
    }

    public void OnEndDrag(ButtonTrigger bt)
    {
        DragImageScript.Instance.Deative();
        if (OnDragEndEvent != null)
            OnDragEndEvent();
    }

    void OnDisable()
    {
        DragImageScript.Instance.Deative();
    }
}
