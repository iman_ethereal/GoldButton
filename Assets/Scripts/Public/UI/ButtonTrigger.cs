﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


public class ButtonTrigger : MonoBehaviour, IPointerClickHandler,IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IPointerEnterHandler
{

    private Animator animator;

    public delegate void DelegateButton( ButtonTrigger bt );
    public delegate void DelegateDrag(Vector2 pos);
    public DelegateButton OnButtonLongClick;
    public DelegateButton OnButtonClicked;
    public DelegateButton OnButtonPointerDown;
    public DelegateButton OnButtonDrag;
    public DelegateButton OnBeginDragEvent;
    public DelegateButton OnEndDragEvent;
    public DelegateButton OnDropEvent;
    public DelegateButton OnButtonDeselect;
    public DelegateDrag OnDragPos;

    private Vector2 curpos;

    public bool intractable = true;

    public float dragTime = .5f;
    public float dragLength = 15;
    private float dragTimer;
    private bool stillClicked;
    private bool pointerDown;
    private bool actioned;

    public bool StillClicked
    {
        get
        {
            return stillClicked;
        }

        set
        {
            stillClicked = value;
        }
    }

    void Start()
    {
        dragTimer = dragTime;
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if( pointerDown )
        {
            if( dragTimer > 0)
            {
                dragTimer -= 1 * Time.deltaTime;
            }
            else
            {
                OnDraged();
            }
        }
    }
    public void OnDraged()
    {
        if (!intractable)
            return;

        if ( !actioned)
        {
            if (OnButtonLongClick != null)
            {
                OnButtonLongClick(this);
                Animating();
            }
            Debug.Log("OnButtonLongClick");
            actioned = true;
        }
        _ResetDrag();
    }

    public void _ResetDrag()
    {
        pointerDown = false;
        dragTimer = dragTime;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick();
    }
    public void OnClick()
    {
        if (!intractable)
            return;

        if (!actioned)
        {
            if (OnButtonClicked != null)
            {
                OnButtonClicked(this);
                Animating();
            }
            actioned = true;

            Debug.Log("OnButtonClicked");
        }
    }

    public void Animating()
    {
        if (animator != null)
            animator.CrossFadeInFixedTime("BeatButton", 0);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnClickDown();
    }
    public void OnClickDown()
    {
        if (!intractable)
            return;

        if (OnButtonPointerDown != null)
            OnButtonPointerDown(this);

        pointerDown = true;
        actioned = false;
        stillClicked = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnClickUp();
    }
    public void OnClickUp()
    {
        if (!intractable)
            return;

        if (OnButtonDeselect != null)
            OnButtonDeselect(this);

        _ResetDrag();
        stillClicked = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        OnExit();
    }

    public void OnExit()
    {
        if (!intractable)
            return;
        if (!stillClicked)
            return;

        if (OnButtonDeselect != null)
            OnButtonDeselect(this);

        _ResetDrag();
        stillClicked = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        OnEndDrag();
    }
    public void OnEndDrag()
    {
        if (!intractable)
            return;

        if (OnEndDragEvent != null)
            OnEndDragEvent(this);

        _ResetDrag();
        stillClicked = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!intractable)
            return;

        if (curpos.x + curpos.y > eventData.position.x + eventData.position.y + dragLength || curpos.x + curpos.y < eventData.position.x + eventData.position.y - dragLength)
        {
            OnDrag();
        }

        if( OnDragPos != null)
        {
            OnDragPos(eventData.position);
        }
    }

    public void OnDrag()
    {
        if (!actioned)
        {
            if (OnButtonDrag != null)
            {
                OnButtonDrag(this);
                Animating();
            }
            actioned = true;
            Debug.Log("OnButtonDrag");
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!intractable)
            return;

        if (OnBeginDragEvent != null)
            OnBeginDragEvent(this);

        curpos = eventData.position;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

    }

    public void OnDrop(PointerEventData eventData)
    {
        if (!intractable)
            return;

        if (OnDropEvent!= null)
            OnDropEvent(this);

        Debug.Log("Droped");
    }
}
public enum AxixType
{
    Left, Right, Up, Down
}
