﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class DragImageScript : Singleton<DragImageScript> {

    public RectTransform rect;
    public Image image;

    public object carriedObject;


    void Start()
    {
        image.enabled = false;
    }

    public void Active( Sprite icon, Vector2 pos, Vector2 delta, object draged )
    {
        image.sprite = icon;
        image.rectTransform.anchoredPosition = pos;
        image.rectTransform.sizeDelta = delta;
        image.enabled = true;
        carriedObject = draged;
    }

    public void SetPosition(Vector2 pos)
    {
        rect.position = new Vector2(pos.x, pos.y + (rect.sizeDelta.y / 3));
    }

    public void Deative()
    {
        image.enabled = false;
        carriedObject = null;
    }

}
