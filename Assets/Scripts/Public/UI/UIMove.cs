﻿using UnityEngine;
using System.Collections;

public class UIMove : MonoBehaviour {

    Rigidbody2D boddy;
    RectTransform rectTransform;
    private float speed = 2;

    public bool horizontal = true;
    public bool vertical = true;

    public RectTransform RectTransform
    {
        get
        {
            return rectTransform;
        }

        set
        {
            rectTransform = value;
        }
    }

    private bool reverse;
    // Use this for initialization
    void Start()
    {
        boddy = GetComponent<Rigidbody2D>();
        RectTransform = GetComponent<RectTransform>();
        reverse = Random.Range(0, 2) == 0;
    }

    public void Shake(float h, float v)
    {
        boddy.AddForce(reverse ? new Vector2(horizontal ? h : 0, vertical ? v : 0) : new Vector2(vertical ? v : 0, horizontal ? h : 0), ForceMode2D.Force);
    }
    public void Move(float h, float v)
    {
        boddy.AddForce(new Vector2(horizontal ? h : 0, vertical ? v : 0), ForceMode2D.Force);
    }

    public void MoveTo( Vector2 pos, Quaternion rot, out bool inpos)
    {
        inpos = false;
        if (Vector2.Distance( rectTransform.anchoredPosition, pos ) > .1f)
            rectTransform.anchoredPosition = Vector2.Lerp(rectTransform.anchoredPosition, pos, speed * Time.deltaTime);
        else
            inpos = true;

        rectTransform.rotation = Quaternion.Lerp(rectTransform.rotation, rot, (speed * 2) * Time.deltaTime);
    }
}
