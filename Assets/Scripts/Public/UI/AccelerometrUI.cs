﻿using UnityEngine;
using System.Collections;

public class AccelerometrUI : MonoBehaviour {

    RectTransform rTransform;

    [Range(0,2)]
    public float speed = 1;

	// Use this for initialization
	void Start () {
        rTransform = GetComponent<RectTransform>();
	}
	
	// Update is called once per frame
	void Update () {
        float h = Debug.isDebugBuild? Input.GetAxis("Horizontal") * (Time.deltaTime + speed): Input.acceleration.x * (Time.deltaTime + speed);
        float v = Debug.isDebugBuild? Input.GetAxis("Vertical") * (Time.deltaTime + speed): Input.acceleration.y * (Time.deltaTime + speed);
        rTransform.Translate(new Vector3(h, v, 0));
    }
}
