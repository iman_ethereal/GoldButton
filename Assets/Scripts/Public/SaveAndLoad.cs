﻿using UnityEngine;
using System.Collections;

public class SaveAndLoad : MonoBehaviour {

    public string Headkey, boddyKey;

    private string curSaveText;
    public string CurSaveText
    {
        get
        {
            return curSaveText;
        }

        set
        {
            curSaveText = value;
        }
    }

    void Awake()
    {
        Load();
        SetInformations();
    }

	// Use this for initialization
	void Start () {
	}

    public void OnApplicationQuit()
    {
        Save();
    }

    public string GetInformation()
    {
        string temp = "";
        temp = Statices.Separator(Statices.SEPARATOR_BETWEEN_CLASSES
            , PlayerData.Instance.info.GetSaveInformation()
            , LevelManager.Instance.GetSaveInformation()
            , FreezerShop.Instance.shop.GetSaveInformation()
            , GoldButton.Instance.GetSaveInformation()
            , DressRoom.Instance.shop.GetSaveInformation()
            , HappyShop.Instance.shop.GetSaveInformation()
            , BedRoom.Instance.shop.GetSaveInformation());
        Debug.Log(DressRoom.Instance.shop.GetSaveInformation());
        return temp;
    }

    public void Save()
    {
        PlayerPrefs.SetString(string.Concat(Headkey, boddyKey), GetInformation());
        PlayerPrefs.Save();
        Debug.Log("Saved Sucsses");
    }

    public void Load()
    {
        CurSaveText = PlayerPrefs.GetString(string.Concat(Headkey, boddyKey));
        Debug.Log("Load Sucsses");
    }

    public void SetInformations()
    {
        if (CurSaveText == "")
            return;

        string[] sp = CurSaveText.Split(new char[] { Statices.SEPARATOR_BETWEEN_CLASSES });

        PlayerData.Instance.info.SetLoadInformation(sp[0]);

        if (!string.IsNullOrEmpty(sp[1]))
        {
            LevelManager.Instance.SetLoadInformation(sp[1]);
        }
        if (!string.IsNullOrEmpty(sp[2]))
        {
            FreezerShop.Instance.shop.SetLoadInformation(sp[2]);
        }
        if (!string.IsNullOrEmpty(sp[3]))
        {
            GoldButton.Instance.SetLoadInformation(sp[3]);
        }
        if (!string.IsNullOrEmpty(sp[4]))
        {
            DressRoom.Instance.shop.SetLoadInformation(sp[4]);
        }
        if( !string.IsNullOrEmpty(sp[5]))
        {
            HappyShop.Instance.shop.SetLoadInformation(sp[5]);
        }
        if (!string.IsNullOrEmpty(sp[6]))
        {
            BedRoom.Instance.shop.SetLoadInformation(sp[6]);
        }
    }
}
