﻿using UnityEngine;
using System.Collections;
public class Statices : MonoBehaviour {

    public const char SEPARATOR_BETWEEN_CLASSES = '/';
    public const char SEPARATOR_BETWEEN_GROUPS = '|';
    public const char SEPARATOR_BETWEEN_ITEMS = '^';
    public const char SEPARATOR_BETWEEN_LISTITEMS = ',';
    public const char SEPARATOR_BETWEEN_ITEMSOFLIST= '.';

    public static string Separator( char separator, params object[] paramets)
    {
        string temp = "";

        for (int cnt = 0; cnt < paramets.Length; cnt++)
        {
            temp += paramets[cnt].ToString() + separator;
        }
        //temp = temp.TrimEnd(new char[] { separator });
        return temp;
    }
}
public static class StringExtentions
{
    public static int ToInt(this string txt)
    {
        int i = 0;
        int.TryParse(txt, out i);
        return i;
    }
}

