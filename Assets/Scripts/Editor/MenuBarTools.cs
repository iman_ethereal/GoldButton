﻿using UnityEngine;
using UnityEditor;
public class MenuBarTools : MonoBehaviour {

    [MenuItem("Nabet/ClearPrefs")]
    static void ClearPrefs()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("PrefsDeleted");
    }
}
