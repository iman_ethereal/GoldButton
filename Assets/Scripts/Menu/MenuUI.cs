﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class MenuUI : Singleton<MenuUI> {

    public Text goldText, roomNameText;

    public Window levelMenu, mainMenu, kitchemMenu, playRoom, dressRoom;
    public GoldButton goldbutton;

    public delegate void DelegateMenu(MenuType mt);
    public event DelegateMenu OnMenuChanged;

    private int curLevelIndex;
    private int maxMenus;
    void Start () {
        ChangeMenu(MenuType.Kitchen);
        PlayerData.Instance.OnGoldChanged += SetGoldToMenu;
        PlayerData.Instance.OnStarChanged += SetStarToMenu;
        PlayerData.Instance.OnEnergyChanged += SetEnergyToMenu;
        SetGoldToMenu(PlayerData.Instance.info.gold);
        SetStarToMenu(PlayerData.Instance.info.star);
        maxMenus = Enum.GetValues(typeof(MenuType)).Length -2;
        OnMenuChanged += MenuChanged;

	}

    private void MenuChanged(MenuType mt)
    {
        if (GoldButton.Instance.state.FindStat() != "Idle")
            GoldButton.Instance.state.GoToState("Idle");
    }

    public void SetEnableGoldButton(bool ac)
    {
        goldbutton.gameObject.SetActive(ac);
    }

    public void ChangeMenu(int i)
    {
        curLevelIndex = i;
        ChangeMenu((MenuType)i);
    }
    public void ChangeMenu(MenuType type)
    {
        levelMenu.Deactive();
        mainMenu.Deactive();
        kitchemMenu.Deactive();
        dressRoom.Deactive();
        playRoom.Deactive();

        switch (type)
        {
            case MenuType.LevelMenu:
                levelMenu.Active();
                SetEnableGoldButton(false);
                break;
            case MenuType.BedRoom:
                mainMenu.Active();
                SetEnableGoldButton(true);
                break;
            case MenuType.PlayRoom:
                playRoom.Active();
                SetEnableGoldButton(true);
                break;
            case MenuType.Kitchen:
                kitchemMenu.Active();
                SetEnableGoldButton(true);
                break;
            case MenuType.DressRoom:
                SetEnableGoldButton(true);
                dressRoom.Active();
                break;
        }
        roomNameText.text = type.ToString();

        if (OnMenuChanged != null)
            OnMenuChanged(type);
    }

    public void NextRoom()
    {
        if (curLevelIndex < maxMenus)
            curLevelIndex++;
        else
            curLevelIndex = 0;
        ChangeMenu(curLevelIndex);
    }

    public void PreviusRoom()
    {
        if (curLevelIndex > 0)
            curLevelIndex--;
        else
            curLevelIndex = maxMenus;
        ChangeMenu(curLevelIndex);
    }

    private void SetGoldToMenu(int i)
    {
        goldText.text = i.ToString();
    }
    private void SetStarToMenu(int i)
    {
        //starText.text = i.ToString();
    }
    private void SetEnergyToMenu(int i)
    {
        //energyText.text = i.ToString();
    }
}

public enum MenuType
{
    Kitchen, PlayRoom, DressRoom, BedRoom, LevelMenu
}