﻿using UnityEngine;
using System.Collections;

public class BookScripts : MonoBehaviour {

    public int id;

    public float duration = 2;
    private float timer;

    private bool reading;

    private ShopInfo info;

    void Start()
    {
        timer = duration;
    }

    void Update()
    {
        if (reading)
        {
            if( timer > 0)
            {
                timer -= 1 * Time.deltaTime;
            }
            else
            {
                GetEnergy();
            }
        }
    }
    
    public void GetEnergy()
    {
        GoldButton.Instance.energy.AddEnergy("PlayAndRead", info.nutrition.energy, null, null);
        timer = duration;
    }

    public void StartReading(ShopInfo info)
    {
        this.info = info;
        reading = true;
        gameObject.SetActive(true);
    }
    public void StopReading()
    {
        reading = false;
        gameObject.SetActive(false);
    }
}
