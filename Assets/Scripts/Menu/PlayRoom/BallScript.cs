﻿using UnityEngine;
using System.Collections;
using System;

public class BallScript : MonoBehaviour {

    public Rigidbody2D rigid;
    public RectTransform rect;
    public ButtonTrigger trigger;
    public string iTag = "SmallBall";
    public int id = 0;
    public bool isDefault;

    private float startTime, factor = 10;
    private Vector3 startPos, direction;
    private Vector3 lastPos;
    private ShopInfo info;

    public void SetInfo(ShopInfo info)
    {
        this.info = info;
    }
    void Start()
    {
        if(!isDefault)
            SetInfo(HappyShop.Instance.shop.FindItem(iTag, id));
        else
            SetInfo(HappyShop.Instance.shop.FindDefaultItem(iTag, id));


        trigger.OnButtonPointerDown += OnClicking;
        trigger.OnButtonDeselect += OnDrop;
    }

    void Update()
    {
        if (rect.localPosition.y < -330)
        {
            Debug.Log("-Y");
            rect.localPosition = new Vector3(rect.position.x, -280, rect.position.z);
        }
        if (rect.localPosition.y > 500)
        {
            Debug.Log("+Y");
            rect.localPosition = new Vector3(rect.position.x, 400, rect.position.z);
        }

        if (rect.localPosition.x < -400)
        {
            Debug.Log("-X");
            rect.localPosition = new Vector3(-350, rect.position.y, rect.position.z);
        }
        if (rect.localPosition.x > 400)
        {
            Debug.Log("+X");
            rect.localPosition = new Vector3(300, rect.position.y, rect.position.z);
        }
    }

    public void OnClicking( ButtonTrigger bt ) {
        startTime = Time.time;
        startPos = Input.mousePosition;
        startPos.z = transform.position.z - Camera.main.transform.position.z;
        startPos = Camera.main.ScreenToWorldPoint(startPos);
    }

    public void OnDrop( ButtonTrigger bt ) {
        var endPos = Input.mousePosition;
        endPos.z = transform.position.z - Camera.main.transform.position.z;
        endPos = Camera.main.ScreenToWorldPoint(endPos);

        direction = endPos - startPos;
        direction.z = direction.magnitude;
        direction /= (Time.time - startTime);
        rigid.AddForce(direction.normalized * factor);
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        GoldButton.Instance.energy.AddEnergy("PlayAndRead", info.nutrition.energy, null, ()=> 
        {
            GoldButton.Instance.state.GoToState("Refuse");
        });
    }
}
