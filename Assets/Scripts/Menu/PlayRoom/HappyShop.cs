﻿using UnityEngine;
using System.Collections;
using System;

public class HappyShop : Singleton<HappyShop>
{
    public Shop shop;
    public Window originPanel;
    public PanelManager panel;
    public ShopInfoContent[] rows;
    public RoomSliderHandler slider;
    public BallScript[] balls;
    public BookScripts[] books;
    private BallScript lastBall;
    void Start()
    {
        MenuUI.Instance.OnMenuChanged += OnMenuChanged;
        shop.OnBuyEvent += OnBuyItem;
        shop.OnSelectEvent += OnSelectItem;
        shop.OnDeselect += OnDeselectItem;
        panel.OnPanelChangeEvent += PanelChanged;
        slider.Set(shop);
        slider.SlideChanged = SlideChanged;
        SetBall("SmallBall", 0, true);
        DeactiveAllBooks();
        slider.DoubleCheck();
    }

    private void OnMenuChanged(MenuType mt)
    {
        DeactiveAllBooks();
    }

    private void SlideChanged(string tag, int id)
    {
        DeselectAllBalls();
        DeactiveAllBooks();
        if( GoldButton.Instance.state.FindStat() != "Idle")
            GoldButton.Instance.state.GoToState("Idle");
        switch (tag)
        {
            case "SmallBall":
                SetBall(tag, id);
                break;
            case "BigBall":
                SetBall(tag, id);
                break;
        }
    }

    private void PanelChanged(string s)
    {
        if(s.Contains("Contents"))
        {
            SetupRow();
        }
    }

    private void OnDeselectItem(Shop shop, string tag, int id)
    {
        Debug.Log(tag);
    }

    private void OnSelectItem(Shop shop, string tag, int id)
    {
        Debug.Log(tag);
    }

    private void OnBuyItem(Shop shop, string tag, int id)
    {
        Debug.Log(tag);
    }

    public void SetBall(string tag ,int id, bool def = false)
    {
        DeselectAllBalls();
        BallScript b = Array.Find(balls, s => s.iTag.Contains(tag) && s.id == id && s.isDefault == def);
        if (lastBall != null)
            b.rect.anchoredPosition = lastBall.rect.anchoredPosition;
        lastBall = b;
        b.gameObject.SetActive(true);
    }

    public void SetBook( int id )
    {
        Debug.Log(id);
        DeactiveAllBooks();
        BookScripts b = Array.Find(books, s => s.id == id);
        b.StartReading(shop.FindItem("Book", id));
    }

    public void DeselectAllBalls()
    {
        for (int cnt = 0; cnt < balls.Length; cnt++)
        {
            balls[cnt].gameObject.SetActive(false);
        }
    }
    public void DeactiveAllBooks()
    {
        for (int cnt = 0; cnt < books.Length; cnt++)
        {
            books[cnt].StopReading();
        }
    }

    public void ActivePanel()
    {
        originPanel.Active();
        SetupRow();
        OpenPanel("Contents");
    }

    public void DeactivePanel()
    {
        originPanel.Deactive();
    }

    public void OpenPanel(string tag)
    {
        panel.OpenPanel(tag);
    }

    public void DeactiveRows()
    {
        foreach (ShopInfoContent item in rows)
        {
            item.Deactive();
        }
    }

    public int GetFreeRow()
    {
        return Array.FindIndex(rows, r => !r.IsActive);
    }

    public void SetupRow()
    {
        DeactiveRows();
        for (int cnt = 0; cnt < shop.infos.Length; cnt++)
        {
            for (int x = 0; x < shop.infos[cnt].items.Length; x++)
            {
                if (shop.infos[cnt].items[x].buyed)
                {
                    rows[GetFreeRow()].Active(shop.infos[cnt].items[x]);
                }
            }
        }
    }
}