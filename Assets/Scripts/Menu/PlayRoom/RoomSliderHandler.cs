﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RoomSliderHandler: MonoBehaviour
{
    public DoubleSlider slider;
    private Shop shop;
    private SliderShopInfo[] infos;

    public DragAndDropTrigger dragTrigger;
    public GameObject SliderItemPanel, leftBtn, rightBtn;
    public delegate void OnSlideChanged( string tag, int id );
    public OnSlideChanged SlideChanged;

    public bool HasItem
    {
        get { return infos.Length > 0; }
    }

    private int index;

    public int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }

    public void Set( Shop s)
    {
        shop = s;
        Converting();
        MenuUI.Instance.OnMenuChanged += MenuChanged;
        dragTrigger.OnDragBeginEvent += OnBeginDragItem;
        shop.OnBuyEvent += OnItemsChanged;
        shop.OnUsed += OnItemsChanged;
        DoubleCheck();
    }
    private void Converting()
    {
        infos = Convert();
    }

    private SliderShopInfo[] Convert()
    {
        List<SliderShopInfo> ilist = new List<SliderShopInfo>();
        for (int cnt = 0; cnt < shop.infos.Length; cnt++)
        {
            for (int x = 0; x < shop.infos[cnt].items.Length; x++)
            {
                if(shop.infos[cnt].items[x].buyed)
                    ilist.Add(new SliderShopInfo() { info = shop.infos[cnt], tag = shop.infos[cnt].tag, id = shop.infos[cnt].items[x].id } );
            }
        }
        return ilist.ToArray();
    }

    private void IncreaseIndex()
    {
        if (index < (infos.Length))
            index++;
    }

    private void DecreaseIndex()
    {
        if (index > 0)
            index--;
    }

    private void OnItemsChanged(Shop shop, string tag, int id)
    {
        this.shop = shop;
        Converting();
        DoubleCheck();
    }

    private void OnBeginDragItem()
    {
        ShopInfo info = shop.FindItem(infos[index].tag, infos[index].id);
        SendShopInfoObject sb = new SendShopInfoObject() { info = info, tag = infos[index].tag };
        dragTrigger.SetObject(sb);
    }

    private void MenuChanged(MenuType mt)
    {
        if (mt == MenuType.Kitchen)
        {
            if (HasItem)
            {
            }
        }
    }

    private void Check()
    {
        leftBtn.SetActive(!(infos.Length -1 == Index));
        rightBtn.SetActive(Index > 0);
        SliderItemPanel.SetActive(HasItem);
    }

    public void GoNext()
    {
        slider.GotoRight(() =>
        {
            SetImages(index +1, index);
            IncreaseIndex();
            Check();
        });
    }


    public void GoPrevius()
    {
        slider.GotoLeft(() =>
        {
            SetImages(index -1, index);
            DecreaseIndex();
            Check();
        });
    }

    public void DoubleCheck()
    {
        if (infos.Length > 0)
        {
            index = infos.Length > 1 ? index : 0;
            ShopInfo Pinfo = shop.FindItem(infos[index].tag, infos[index].id);
            slider.SetPrimary(Pinfo.icon);
            if (SlideChanged != null)
                SlideChanged(infos[index].tag, infos[index].id);
        }
        Converting();
        Check();
    }

    private void SetImages(int a, int s)
    {
        if (infos.Length > 0)
        {
            if (a < infos.Length || a > 0)
            {
                ShopInfo Pinfo = shop.FindItem( infos[a].tag, infos[a].id);
                ShopInfo Sinfo = shop.FindItem( infos[s].tag ,infos[s].id);
                slider.SetImages(Pinfo.icon, Sinfo.icon);
                if (SlideChanged != null)
                    SlideChanged(infos[a].tag ,infos[a].id);
            }
        }
    }
}
[Serializable]
public class SliderShopInfo
{
    public ShopBaseInfo info;
    public int id;
    public string tag;
}
public class SendShopInfoObject
{
    public string tag;
    public ShopInfo info;
}
