﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FreezerShopContents : MonoBehaviour {

    public Text nameText, countText;
    public Image iconSlot;

    public bool IsActive
    {
        get { return gameObject.activeSelf; }
    }
    public void Active( ShopInfo info )
    {
        gameObject.SetActive(true);
        nameText.text = info.name;
        countText.text = info.count.ToString();
        iconSlot.sprite = info.icon;
    }

    public void Deactive()
    {
        gameObject.SetActive(false);
    }
}
