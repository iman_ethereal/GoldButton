﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class FreezerShop : Singleton<FreezerShop> {

    public Shop shop;
    public Window mainPanel;
    public RectTransform contentPanel;
    public FreezerShopContents[] rows;
    public PanelManager panel;
    public RoomSliderHandler slider;
    public Text countText;

    public const int WindowSortNumber = 20;

    void Start()
    {
        slider.SlideChanged = OnSlideChange;
        slider.Set(shop);
        shop.OnBuyEvent += OnItemBuyed;
        shop.OnUsed += ONItemUsed;
        panel.OnPanelChangeEvent += PanelChanged; MenuUI.Instance.OnMenuChanged += MenuChange;
        CloseFreezer();
    }

    private void ONItemUsed(Shop shop, string tag, int id)
    {
        
    }

    private void OnItemBuyed(Shop shop, string tag, int id)
    {

    }

    private void OnSlideChange(string tag, int id)
    {
        ShopInfo info = shop.FindItem(tag, id);
        countText.text = info.count.ToString();
    }

    private void PanelChanged(string obj)
    {
        switch (obj)
        {
            case "IceBox":
                SetupFreezer();
                break;
        }
    }

    private void MenuChange(MenuType mt)
    {
        if( mt == MenuType.Kitchen)
        {
            CloseFreezer();
        }
    }

    public void OpenFreezer() {
        if (!mainPanel.isActive)
        {
            mainPanel.Active();
            DeactiveAll();
            SetupFreezer();
            panel.OpenPanel("IceBox");
        }
    }
    public void CloseFreezer()
    {
        if (mainPanel.isActive)
            mainPanel.Deactive();
    }

    public void SetupFreezer()
    {
        DeactiveAll();
        int lastIndex = 0;
        for (int cnt = 0; cnt < shop.infos.Length; cnt++)
        {
            for (int x = 0; x < shop.infos[cnt].items.Length; x++)
            {
                if (shop.infos[cnt].items[x].buyed)
                {
                    GetRow().Active(shop.infos[cnt].items[x]);
                    lastIndex = cnt;
                }
            }
        }
        contentPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Mathf.Abs(rows[lastIndex].GetComponent<RectTransform>().localPosition.y) + 200);
    }

    public void DeactiveAll()
    {
        for (int cnt = 0; cnt < rows.Length; cnt++)
        {
            rows[cnt].Deactive();
        }
    }

    public void OpenPanel(string tag)
    {
        panel.OpenPanel(tag);
    }

    public FreezerShopContents GetRow()
    {
        int index = Array.FindIndex(rows, r => !r.IsActive);
        return rows[index];
    }
}
