﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class FreezerItemsUIHandler : MonoBehaviour {

    public DoubleSlider slider;
    public FreezerItems freezer;
    public DragAndDropTrigger dragTrigger;
    public GameObject SliderItemPanel, leftBtn, rightBtn;
    public Text countText;

    public bool HasItem
    {
        get { return FreezerItems.Length > 0; }
    }

    public FreezerSlotInfo[] FreezerItems
    {
        get { return freezer.freezerItemList.ToArray(); }
    }

    private int index;

    public int Index
    {
        get
        {
            return index;
        }

        set
        {
            index = value;
        }
    }

    public void Start()
    {
        MenuUI.Instance.OnMenuChanged += MenuChanged;

        dragTrigger.OnDragBeginEvent += OnBeginDragItem;
        freezer.OnItemAdded += OnItemsChanged;
        freezer.OnItemRemove += OnItemsChanged;
        
        DoubleCheck();
    }

    public int GetIndex()
    {
        return Mathf.Clamp(index, 0, FreezerItems.Length);
    }

    public void IncreaseIndex()
    {
        if (index < (FreezerItems.Length))
            index++;
    }

    public void DecreaseIndex()
    {
        if (index > 0)
            index--;
    }

    private void OnItemsChanged()
    {
        DoubleCheck();
    }

    private void OnBeginDragItem()
    {
        FreezerSlotInfo Pinfo = freezer.FindSlot(FreezerItems[Index].id);
        dragTrigger.SetObject(Pinfo);
    }

    private void MenuChanged(MenuType mt)
    {
        if( mt== MenuType.Kitchen)
        {
            if (HasItem)
            {
                //DoubleCheck();
            }
        }
    }

    public void Check()
    {
        leftBtn.SetActive(FreezerItems.Length - 1 > Index);
        rightBtn.SetActive(Index > 0);
        SliderItemPanel.SetActive(HasItem);
    }

    public void GoNext()
    {
        slider.GotoRight( ()=>
        {
            SetImages(GetIndex() + 1, GetIndex());
            IncreaseIndex();
            Check();
        });
    }


    public void GoPrevius()
    {
        slider.GotoLeft(() =>
        {
            SetImages(GetIndex() - 1, GetIndex());
            DecreaseIndex();
            Check();
        });
    }

    public void DoubleCheck()
    {
        if( FreezerItems.Length > 0)
        {
            index = FreezerItems.Length > 1 ? GetIndex() : 0;
            FreezerItemInfo Pinfo = freezer.FindItem(FreezerItems[GetIndex()].id);
            slider.SetPrimary(Pinfo.icon);
            countText.text =  FreezerItems[GetIndex()].count.ToString();
        }
        Check();
    }

    public void SetImages(int a, int s)
    {
        if (FreezerItems.Length > 0)
        {
            if (a < FreezerItems.Length || a > 0)
            {
                FreezerItemInfo Pinfo = freezer.FindItem(FreezerItems[a].id);
                FreezerItemInfo Sinfo = freezer.FindItem(FreezerItems[s].id);
                slider.SetImages(Pinfo.icon, Sinfo.icon);
                countText.text = FreezerItems[a].count.ToString();
            }
        }
    }
}
