﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FreezerBuyItemButton : MonoBehaviour {

    public Text priceText, energyText, nameText;

    public string iTag;
    public int id;

    void Start()
    {
        ShopInfo item = FreezerShop.Instance.shop.FindItem(iTag,id);
        priceText.text = item.nutrition.gold.ToString();
        energyText.text = item.nutrition.energy.ToString();
        nameText.text = item.name;
    }

    public void OnClick()
    {
        ShopInfo item = FreezerShop.Instance.shop.FindItem(iTag, id);
        PlayerData.Instance.BuyWithgold(item.nutrition.gold, () =>
        {
            FreezerShop.Instance.shop.Buy(iTag, id);
        }, null);
    }
}
