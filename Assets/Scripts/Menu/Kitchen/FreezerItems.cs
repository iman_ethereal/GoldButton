﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FreezerItems : MonoBehaviour {

    public List<FreezerSlotInfo> freezerItemList = new List<FreezerSlotInfo>();
    public FreezerItemInfo[] items;
    public delegate void DelegateFreezer();
    public event DelegateFreezer OnItemAdded;
    public event DelegateFreezer OnItemRemove;

    public string GetSaveInformation()
    {
        string temp = "";
        for (int cnt = 0; cnt < freezerItemList.Count; cnt++)
        {
            string s = string.Concat(Statices.Separator(Statices.SEPARATOR_BETWEEN_ITEMSOFLIST, freezerItemList[cnt].id, freezerItemList[cnt].count));
            temp += string.Concat(s, Statices.SEPARATOR_BETWEEN_LISTITEMS);
        }
        temp = temp.TrimEnd(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        return temp;
    }

    public void SetLoadInformation(string saveData)
    {
        string[] sp = saveData.Split(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        for (int cnt = 0; cnt < sp.Length; cnt++)
        {
            string[] spItems = sp[cnt].Split(new char[] { Statices.SEPARATOR_BETWEEN_ITEMSOFLIST });
            FreezerSlotInfo tempSlot = new FreezerSlotInfo() { id = spItems[0].ToInt(), count = spItems[1].ToInt() };
            AddSlot(tempSlot);
        }
    }

    public void AddSlot(FreezerSlotInfo info)
    {
        freezerItemList.Add(info);
    }

    public FreezerSlotInfo FindSlot(int id)
    {
        return freezerItemList.Find(i => i.id == id);
    }

    public FreezerItemInfo FindItem(int id)
    {
        return Array.Find(items, i => i.id == id);
    }

    public void AddItem(int id)
    {
        FreezerSlotInfo newSlot = FindSlot(id);
        if (newSlot == null)
        {
            newSlot = new FreezerSlotInfo() { id = id };
            AddSlot(newSlot);
        }
        newSlot.AddItem();
        if (OnItemAdded != null)
            OnItemAdded();
    }

    public void RemoveItem(int id)
    {
        FreezerSlotInfo newSlot = FindSlot(id);
        newSlot.RemoveItem();
        if (newSlot.count <= 0)
        {
            freezerItemList.Remove(newSlot);
        }
        if (OnItemRemove != null)
            OnItemRemove();
    }
}
[Serializable]
public class FreezerSlotInfo
{
    public int id;
    public int count;

    public void AddItem()
    {
        count++;
    }

    public void RemoveItem()
    {
        if (count > 0)
            count--;
    }
}

[Serializable]
public class FreezerItemInfo
{
    public string tag;
    public FreezerItemCategory Category;
    public string itemName;
    public int id;
    public Sprite icon;
    public Nutrition nutrition;
}
public enum FreezerItemCategory
{
    Biscuit, Chocolate, Soup, Vegetables, Food, Drink, Fruit
}
