﻿using UnityEngine;
using System.Collections;
using System;

public class DressRoom : Singleton<DressRoom> {

    public Shop shop;
    public PanelManager panel;
    public ShopInfoContent[] rows;
    public Window originPanel;
    public RoomSliderHandler slider;
    public ItemDesarmamentButton desarmamenter;

    void Start()
    {
        MenuUI.Instance.OnMenuChanged += OnMenuChanged;
        shop.OnBuyEvent += OnBuyItem;
        shop.OnSelectEvent += OnSelectItem;
        shop.OnDeselect += OnDeselectItem;
        panel.OnPanelChangeEvent += PanelChanged;
        DeactiveOrginPanel();
        CheckContetns();
        slider.Set(shop);
        slider.SlideChanged = SlideChanged;
        slider.DoubleCheck();
    }

    private void SlideChanged(string tag, int id)
    {
        DesarmamentButtonCheck(id, tag);
    }

    public void DesarmamentButtonCheck( int id, string tag )
    {
        ShopInfo s = shop.FindItem(tag, id);
        if (s.select)
        {
            desarmamenter.Active(id, tag);
        }
        else
            desarmamenter.Deactive();
    }

    public void BuyContetn(string tag, int id)
    {
        shop.Buy(tag, id);
    }

    public void SelectContent(string tag, int id)
    {
        shop.Select(tag, id);
    }

    private void PanelChanged(string s)
    {
        CheckContetns();
    }

    private void OnDeselectItem(Shop shop, string tag, int id)
    {
        DesarmamentButtonCheck(id, tag);
    }

    private void OnSelectItem(Shop shop, string tag, int id)
    {
        DesarmamentButtonCheck(id, tag);
    }

    private void OnBuyItem(Shop shop, string tag, int id)
    {
        CheckContetns();
        DesarmamentButtonCheck(id, tag);
    }

    private void OnMenuChanged(MenuType mt)
    {
        if( mt == MenuType.DressRoom)
            CheckContetns();
    }

    public void CheckContetns()
    {
        for (int cnt = 0; cnt < rows.Length; cnt++)
        {
            rows[cnt].Deactive();
        }

        for (int cnt = 0; cnt < shop.infos.Length; cnt++)
        {
            for (int x = 0; x < shop.infos[cnt].items.Length; x++)
            {
                if( shop.infos[cnt].items[x].buyed)
                {
                    GetFreeRow().Active(shop.infos[cnt].items[x]);
                }
            }
        }
    }

    public ShopInfoContent GetFreeRow()
    {
        return Array.Find(rows, r => !r.IsActive);
    }

    public void ActiveOriginPanel()
    {
        originPanel.Active();
        OpenPanel("Content");
    }
    public void DeactiveOrginPanel()
    {
        originPanel.Deactive();
    }

    public void OpenPanel(string tag)
    {
        panel.OpenPanel(tag);
    }
}
