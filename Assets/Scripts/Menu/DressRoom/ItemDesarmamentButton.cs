﻿using UnityEngine;
using System.Collections;

public class ItemDesarmamentButton : MonoBehaviour {

    private int id;
    private string iTag;

    void Start()
    {
        Deactive();
    }

    public void Active( int id, string iTag )
    {
        this.id = id;
        this.iTag = iTag;
        gameObject.SetActive(true);
    }

    public void Deactive()
    {
        gameObject.SetActive(false);
    }

    public void OnClick()
    {
        DressRoom.Instance.shop.Deselect(iTag, id);
        GoldButton.Instance.customize.EquipAll();
    }
}
