﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class EnergyUIHandler : Singleton<EnergyUIHandler> {

    public Slider slider;
    public EnergyButton playEnergyBtn, eatEnergyBtn, clothingBtn, playAndReadBtn;
    public Window contentPanel;
    private Char_Energy char_energy;

    void Start()
    {
        char_energy = GoldButton.Instance.energy;
        slider.maxValue = char_energy.MaxEnergy;
        char_energy.OnEnergyChanged += SetBaseEnergy;
        SetBaseEnergy(char_energy.CurEnergy);
        SetupEnergies();
        ShowPanel(false);
    }

    public void ShowPanel(bool ac)
    {
        if (ac)
        {
            contentPanel.Active();
            SetupEnergies();
        }
        else
            contentPanel.Deactive();
    }

    public void SetupEnergies()
    {
        playEnergyBtn.Set(char_energy.energys[char_energy.FindEnergy("Sleep")].maxEnergy, char_energy.energys[char_energy.FindEnergy("Sleep")].curEnergy);
        clothingBtn.Set(char_energy.energys[char_energy.FindEnergy("Clothing")].maxEnergy, char_energy.energys[char_energy.FindEnergy("Clothing")].curEnergy);
        eatEnergyBtn.Set(char_energy.energys[char_energy.FindEnergy("Eat")].maxEnergy, char_energy.energys[char_energy.FindEnergy("Eat")].curEnergy);
        playAndReadBtn.Set(char_energy.energys[char_energy.FindEnergy("PlayAndRead")].maxEnergy, char_energy.energys[char_energy.FindEnergy("PlayAndRead")].curEnergy);
    }

    public void SetBaseEnergy(float g)
    {
        slider.minValue = 0;
        slider.value = g;
    }
}
