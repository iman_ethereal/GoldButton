﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnergyButton : MonoBehaviour {

    public Slider slider;

    public void Set( float max, float value )
    {
        slider.minValue = 0;
        slider.maxValue = max;
        slider.value = value;
    }
}
