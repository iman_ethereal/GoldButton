﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class LevelButton : MonoBehaviour {

    private LevelCommand level;

    public void SetCommand( LevelCommand c)
    {
        level = c;
    }

    void Update()
    {
        if (level != null)
            if (level.baseValues.active)
                level.Update();
    }

    public void Deactive()
    {
        level.Exit();
        level.Deactived();
        gameObject.SetActive(false);
    }

    public void Active()
    {
        level.Setup();
        level.Activated();
        gameObject.SetActive(true);
    }
}
