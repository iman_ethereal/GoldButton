﻿using UnityEngine;
using System;

public abstract class LevelCommand {
    public abstract void Sucsess();
    public abstract void Failer();
    public abstract void Setup();
    public abstract void Update();
    public abstract void Exit();
    public void Activated()
    {
        baseValues.active = true;
    }
    public void Deactived()
    {
        baseValues.active = false;
    }
    public CommandBaseValaues baseValues;
}
[Serializable]
public class CommandBaseValaues
{
    public int index;
    public AudioClip tooltipAudio;
    public LevelButton button;
    public MetabolismInfo meta;
    public bool active;
}

public abstract class UsedCommand
{
    GoldButton _goldButton;
    protected GoldButton GoldButton
    {
        set { _goldButton = value; }
        get { return _goldButton; }
    }

    public UsedCommand(GoldButton gb)
    {
        _goldButton = gb;
    }

    public abstract void Activate(SendShopInfoObject info);
}