﻿using UnityEngine;
using System.Collections;
using System;
public class LevelManager : Singleton<LevelManager> {


    public LevelInfo[] levels;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public string GetSaveInformation()
    {
        return Statices.Separator(',', GetPathedLevel());
    }

    public int GetPathedLevel()
    {
        int i = 0;
        for (int cnt = 0; cnt < levels.Length; cnt++)
        {
            if (levels[cnt].sucsess)
                i++;
        }
        return i;
    }

    public void SetLoadInformation( string txt )
    {
        string[] sp = txt.Split(new char[] { ',' });
        for (int cnt = 0; cnt < sp[0].ToInt(); cnt++)
        {
            levels[cnt].Sucsessed();
        }
    }

    public void NextLevel()
    {
        levels[GetPathedLevel()].Sucsessed();
        Debug.Log("GoNext: " + GetPathedLevel());
    }

    public void PreviousLevel()
    {
        levels[GetPathedLevel() -1].Reset();
    }

    public void OnLevelSucsess(MetabolismInfo meta)
    {
        PlayerData.Instance.IncreaseGold(meta.gold);
        PlayerData.Instance.IncreaseStar(meta.star);
        GoldButton.Instance.energy.ReductEnergy(meta.energy);
        NextLevel();
        MenuLevel.Instance.Check();

        Debug.Log("Reduct Energy : " + meta.energy);
    }

    public void OnLevelFailed(MetabolismInfo meta)
    {
        GoldButton.Instance.energy.ReductEnergy(meta.energy);
    }
}
[Serializable]
public class LevelInfo
{
    public int id;
    public string levelName;
    public MetabolismInfo metaInfo;
    public bool sucsess;
    public void Sucsessed()
    {
        sucsess = true;
    }
    public void Reset()
    {
        sucsess = false;
    }
}

[Serializable]
public class MetabolismInfo
{
    public int star;
    public int energy;
    public int gold;
}
