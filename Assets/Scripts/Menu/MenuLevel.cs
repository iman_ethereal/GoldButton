﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class MenuLevel : MonoBehaviour {
    private static MenuLevel instance;

    public static MenuLevel Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
    }

    public List<LevelCommand> levelList = new List<LevelCommand>();
    public AudioSource source;

    public Level0 level0;
    public Level1 level1;
    public Level2 level2;
    public Level3 level3;
    public Level4 level4;
    public Level5 level5;
    public Level6 level6;
    public Level7 level7;
    public Level8 level8;
    public Level9 level9;
    public Level10 level10;
    public Level11 level11;
    public Level12 level12;
    public Level13 level13;
    public Level14 level14;
    public Level15 level15;
    public Level16 level16;
    public Level17 level17;
    public Level18 level18;
    public Level19 level19;
    public Level20 level20;
    public Level21 level21;
    public Level22 level22;
    public Level23 level23;
    public Level24 level24;
    public Level25 level25;
    public Level26 level26;


    void Start()
    {
        MenuUI.Instance.OnMenuChanged += MenuChanged;
        AddLevels();
    }

    void Update()
    {
    }

    public void AddLevels()
    {
        levelList.Add(level0);
        levelList.Add(level1);
        levelList.Add(level2);
        levelList.Add(level3);
        levelList.Add(level4);
        levelList.Add(level5);
        levelList.Add(level6);
        levelList.Add(level7);
        levelList.Add(level8);
        levelList.Add(level9);
        levelList.Add(level10);
        levelList.Add(level11);
        levelList.Add(level12);
        levelList.Add(level13);
        levelList.Add(level14);
        levelList.Add(level15);
        levelList.Add(level16);
        levelList.Add(level17);
        levelList.Add(level18);
        levelList.Add(level19);
        levelList.Add(level20);
        levelList.Add(level21);
        levelList.Add(level22);
        levelList.Add(level23);
        levelList.Add(level24);
        levelList.Add(level25);
        levelList.Add(level26);

        for (int cnt = 0; cnt < levelList.Count; cnt++)
        {
            levelList[cnt].baseValues.button.SetCommand(levelList[cnt]);
        }
    }

    private void MenuChanged(MenuType mt)
    {
        if( mt == MenuType.LevelMenu)
        {
            Check();
        }
    }

    public void Check()
    {
        int i = 0;
        for (int cnt = 0; cnt < levelList.Count; cnt++)
        {
            if (levelList[cnt].baseValues.index != LevelManager.Instance.GetPathedLevel())
                levelList[cnt].baseValues.button.Deactive();
            else
                i = cnt;
        }
        levelList[i].baseValues.button.Active();
    }

    public void NextLevel()
    {
        LevelManager.Instance.NextLevel();
        Check();
    }

    public void PreviousLevel()
    {
        LevelManager.Instance.PreviousLevel();
        Check();
    }

}

[Serializable]
public class Level0 : LevelCommand
{
    public ButtonTrigger btn1;
    public ButtonTrigger btn2;
    private List<ButtonTrigger> bts = new List<ButtonTrigger>();
    private int clickCount;

    public override void Exit()
    {
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        Input.multiTouchEnabled = true;
        btn1.OnButtonPointerDown = OnClicked;
        btn2.OnButtonPointerDown = OnClicked;
        btn1.OnButtonDeselect = Deselected;
        btn2.OnButtonDeselect = Deselected;
        Deselected(btn1);
        Deselected(btn2);
    }

    private void Deselected(ButtonTrigger bt)
    {
        if (bts.Contains(bt))
            bts.Remove(bt);

        bt.gameObject.GetComponent<Image>().color = Color.white;
    }

    private void OnClicked(ButtonTrigger bt)
    {
        bt.gameObject.GetComponent<Image>().color = Color.red;
        if (!bts.Contains(bt))
            bts.Add(bt);

        if (bts.Count >= 2)
            Sucsess();
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if( Input.GetKeyDown(KeyCode.W))
        {
            OnClicked(btn1);
        }
    }
}
[Serializable]
public class Level1 : LevelCommand
{
    public ButtonTrigger trigger1;

    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        trigger1.OnButtonDrag = OnDraged;
    }

    private void OnDraged(ButtonTrigger bt)
    {
        Sucsess();
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level2 : LevelCommand
{
    public ButtonTrigger trigger1;
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        trigger1.OnButtonDrag = OnDraged;
    }

    private void OnDraged(ButtonTrigger bt)
    {
        Sucsess();
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level3 : LevelCommand
{
    public ButtonTrigger trigger1;

    public GameObject[] btns;
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        trigger1.OnButtonClicked = OnClicked;
        ResetButtons();
        i = 0;
    }
    public void ResetButtons()
    {
        for (int cnt = 0; cnt < btns.Length; cnt++)
        {
            btns[cnt].SetActive(false);
        }
    }

    int i;
    private void OnClicked(ButtonTrigger bt)
    {
        //trigger1.GetComponent<Animator>().CrossFadeInFixedTime("BeatButton", 0);
        i++;
        if (i > btns.Length)
        {
            for (int cnt = 0; cnt < btns.Length; cnt++)
            {
                btns[cnt].SetActive(true);
            }
            Sucsess();
        }
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level4 : LevelCommand
{
    public ButtonTrigger trigger1;

    public GameObject[] btns;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        trigger1.OnButtonClicked = OnClicked;
        ResetButtons();
        i = 0;
    }
    public void ResetButtons()
    {
        for (int cnt = 0; cnt < btns.Length; cnt++)
        {
            btns[cnt].SetActive(false);
        }
    }

    int i;
    private void OnClicked(ButtonTrigger bt)
    {
        //trigger1.GetComponent<Animator>().CrossFadeInFixedTime("BeatButton", 0);
        i++;
        if (i > btns.Length)
        {
            for (int cnt = 0; cnt < btns.Length; cnt++)
            {
                btns[cnt].SetActive(true);
            }
            Sucsess();
        }
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level5 : LevelCommand
{
    public ButtonTrigger trigger1;

    public GameObject[] btns;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        trigger1.OnButtonClicked = OnClicked;
        ResetButtons();
        i = 0;
    }
    public void ResetButtons()
    {
        for (int cnt = 0; cnt < btns.Length; cnt++)
        {
            btns[cnt].SetActive(false);
        }
    }

    int i;
    private void OnClicked(ButtonTrigger bt)
    {
        //trigger1.GetComponent<Animator>().CrossFadeInFixedTime("BeatButton", 0);
        i++;
        if (i > btns.Length)
        {
            for (int cnt = 0; cnt < btns.Length; cnt++)
            {
                btns[cnt].SetActive(true);
            }
            Sucsess();
        }
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level6 : LevelCommand
{
    public UIMove[] rbmus;
    public RectTransform[] rects;
    public float speed = 1;
    public float shakeTime = 4;
    public float shakeTimer;
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = rects[cnt].rotation;
        }
        shakeTimer = 0;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if (shakeTimer >= shakeTime)
            return;

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            if (rbmus[cnt].RectTransform.anchoredPosition.x <= -450
                || rbmus[cnt].RectTransform.anchoredPosition.x >= 450
                || rbmus[cnt].RectTransform.anchoredPosition.y <= -280
                || rbmus[cnt].RectTransform.anchoredPosition.y >= 280)
            {
                rbmus[cnt].RectTransform.anchoredPosition = rects[cnt].anchoredPosition;
            }
        }

        float h = Debug.isDebugBuild ? Input.GetAxis("Horizontal") * (Time.deltaTime + speed) : Input.acceleration.x * (Time.deltaTime + speed);
        float v = Debug.isDebugBuild ? Input.GetAxis("Vertical") * (Time.deltaTime + speed) : Input.acceleration.y * (Time.deltaTime + speed);
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].Shake(h, v);
        }
        if( h + v !=  0)
        {
            shakeTimer += 1 * Time.deltaTime;
        }
        if (shakeTimer >= shakeTime)
            Sucsess();
    }
}
[Serializable]
public class Level7 : LevelCommand
{
    public UIMove[] rbmus;
    public RectTransform[] rects;
    public float speed = 1;
    public float shakeTime = 4;
    public float shakeTimer;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = rects[cnt].rotation;
        }
        shakeTimer = 0;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if (shakeTimer >= shakeTime)
            return;

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            if( rbmus[cnt].RectTransform.anchoredPosition.x <= -450 
                || rbmus[cnt].RectTransform.anchoredPosition.x >= 450
                || rbmus[cnt].RectTransform.anchoredPosition.y <= -280
                || rbmus[cnt].RectTransform.anchoredPosition.y >= 280)
            {
                rbmus[cnt].RectTransform.anchoredPosition = rects[cnt].anchoredPosition;
            }
        }

        float h = Debug.isDebugBuild ? Input.GetAxis("Horizontal") * (Time.deltaTime + speed) : Input.acceleration.x * (Time.deltaTime + speed);
        float v = Debug.isDebugBuild ? Input.GetAxis("Vertical") * (Time.deltaTime + speed) : Input.acceleration.y * (Time.deltaTime + speed);
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].Shake(h, v);
        }
        if (h + v != 0)
        {
            shakeTimer += 1 * Time.deltaTime;
        }
        if (shakeTimer >= shakeTime)
            Sucsess();
    }
}
[Serializable]
public class Level8 : LevelCommand
{
    public UIMove[] rbmus;
    public RectTransform[] rects;
    public float speed = 1;
    public float shakeTime = 4;
    public float shakeTimer;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = rects[cnt].rotation;
        }
        shakeTimer = 0;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if (shakeTimer >= shakeTime)
            return;

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            if (rbmus[cnt].RectTransform.anchoredPosition.x <= -450
                || rbmus[cnt].RectTransform.anchoredPosition.x >= 450
                || rbmus[cnt].RectTransform.anchoredPosition.y <= -280
                || rbmus[cnt].RectTransform.anchoredPosition.y >= 280)
            {
                rbmus[cnt].RectTransform.anchoredPosition = rects[cnt].anchoredPosition;
            }
        }

        float h = Debug.isDebugBuild ? Input.GetAxis("Horizontal") * (Time.deltaTime + speed) : Input.acceleration.x * (Time.deltaTime + speed);

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].Move(h, 0);
        }
        if (h < 0)
        {
            shakeTimer += 1 * Time.deltaTime;
        }
        if (shakeTimer >= shakeTime)
            Sucsess();
    }
}
[Serializable]
public class Level9 : LevelCommand
{
    public UIMove[] rbmus;
    public RectTransform[] rects;
    public float speed = 1;
    public float shakeTime = 4;
    public float shakeTimer;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = rects[cnt].rotation;
        }
        shakeTimer = 0;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if (shakeTimer >= shakeTime)
            return;

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            if (rbmus[cnt].RectTransform.anchoredPosition.x <= -450
                || rbmus[cnt].RectTransform.anchoredPosition.x >= 450
                || rbmus[cnt].RectTransform.anchoredPosition.y <= -280
                || rbmus[cnt].RectTransform.anchoredPosition.y >= 280)
            {
                rbmus[cnt].RectTransform.anchoredPosition = rects[cnt].anchoredPosition;
            }
        }

        float h = Debug.isDebugBuild ? Input.GetAxis("Horizontal") * (Time.deltaTime + speed) : Input.acceleration.x * (Time.deltaTime + speed);

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].Move(h, 0);
        }
        if (h > 0)
        {
            shakeTimer += 1 * Time.deltaTime;
        }
        if (shakeTimer >= shakeTime)
            Sucsess();
    }
}
[Serializable]
public class Level10 : LevelCommand
{
    public UIMove[] rbmus;
    public RectTransform[] rects;
    public float speed = 1;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = rects[cnt].rotation;
        }
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        float h = Debug.isDebugBuild ? Input.GetAxis("Horizontal") * (Time.deltaTime + speed) : Input.acceleration.x * (Time.deltaTime + speed);
        float v = Debug.isDebugBuild ? Input.GetAxis("Vertical") * (Time.deltaTime + speed) : Input.acceleration.y * (Time.deltaTime + speed);

        int inposes = 0;
        if (h + v != 0)
        {
            for (int cnt = 0; cnt < rbmus.Length; cnt++)
            {
                bool inpos;
                rbmus[cnt].MoveTo( MenuLevel.Instance.level6.rects[cnt].anchoredPosition, MenuLevel.Instance.level6.rects[cnt].rotation, out inpos);
                if (inpos)
                {
                    inposes++;
                    if (inposes >= rbmus.Length)
                    {
                        Sucsess();
                    }
                }
            }
        }
        
    }
}
[Serializable]
public class Level11 : LevelCommand
{
    public ButtonTrigger[] triggers;
    private int yellowCicked = 0;
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < triggers.Length; cnt++)
        {
            triggers[cnt].OnButtonClicked = OnClicked;
            triggers[cnt].intractable = true;
        }
        yellowCicked = 0;
    }

    private void OnClicked(ButtonTrigger bt)
    {
        yellowCicked++;
        if (yellowCicked >= triggers.Length)
            Sucsess();
        bt.intractable = false;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level12 : LevelCommand
{
    public ButtonTrigger[] triggers;
    private int yellowCicked = 0;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < triggers.Length; cnt++)
        {
            triggers[cnt].OnButtonClicked = OnClicked;
            triggers[cnt].intractable = true;
        }
        yellowCicked = 0;
    }

    private void OnClicked(ButtonTrigger bt)
    {
        yellowCicked++;
        if (yellowCicked >= triggers.Length)
            Sucsess();
        bt.intractable = false;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {

    }
}
[Serializable]
public class Level13 : LevelCommand
{
    public ButtonTrigger[] triggers;
    private int yellowCicked = 0;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < triggers.Length; cnt++)
        {
            triggers[cnt].OnButtonLongClick = OnClicked;
            triggers[cnt].intractable = true;
        }
        yellowCicked = 0;
    }

    private void OnClicked(ButtonTrigger bt)
    {
        yellowCicked++;
        if (yellowCicked >= triggers.Length)
            Sucsess();
        bt.intractable = false;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {

    }
}
[Serializable]
public class Level14 : LevelCommand
{
    public UIMove[] rbmus;
    public float speed = 1;
    public float shakeTime = 4;
    public float shakeTimer;
    public override void Exit()
    {

    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = MenuLevel.Instance.level6.rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = MenuLevel.Instance.level6.rects[cnt].rotation;
        }
        shakeTimer = 0;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if (shakeTimer >= shakeTime)
            return;

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            if (rbmus[cnt].RectTransform.anchoredPosition.x <= -450
                || rbmus[cnt].RectTransform.anchoredPosition.x >= 450
                || rbmus[cnt].RectTransform.anchoredPosition.y <= -280
                || rbmus[cnt].RectTransform.anchoredPosition.y >= 280)
            {
                rbmus[cnt].RectTransform.anchoredPosition = MenuLevel.Instance.level6.rects[cnt].anchoredPosition;
            }
        }

        float v = Debug.isDebugBuild ? Input.GetAxis("Vertical") * (Time.deltaTime + speed) : Input.acceleration.y * (Time.deltaTime + speed);

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].Move(0, v);
        }
        if (v > 0)
        {
            shakeTimer += 1 * Time.deltaTime;

            if (shakeTimer >= shakeTime)
                Sucsess();
        }

    }
}
[Serializable]
public class Level15 : LevelCommand
{
    public UIMove[] rbmus;
    public RectTransform[] rects;
    public float speed = 1;
    public float shakeTime = 4;
    public float shakeTimer;
    public override void Exit()
    {
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].GetComponent<RectTransform>().anchoredPosition = rects[cnt].anchoredPosition;
            rbmus[cnt].GetComponent<RectTransform>().rotation = rects[cnt].rotation;
        }
        shakeTimer = 0;
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        if (shakeTimer >= shakeTime)
            return;

        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            if (rbmus[cnt].RectTransform.anchoredPosition.x <= -450
                || rbmus[cnt].RectTransform.anchoredPosition.x >= 450
                || rbmus[cnt].RectTransform.anchoredPosition.y <= -280
                || rbmus[cnt].RectTransform.anchoredPosition.y >= 280)
            {
                rbmus[cnt].RectTransform.anchoredPosition = rects[cnt].anchoredPosition;
            }
        }
        
        float v = Debug.isDebugBuild ? Input.GetAxis("Vertical") * (Time.deltaTime + speed) : Input.acceleration.y * (Time.deltaTime + speed);
        for (int cnt = 0; cnt < rbmus.Length; cnt++)
        {
            rbmus[cnt].Shake(0, v);
        }
        if ( v > 0)
        {
            shakeTimer += 1 * Time.deltaTime;
            if (shakeTimer >= shakeTime)
                Sucsess();
        }
    }
}
[Serializable]
public class Level16 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level17 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level18 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level19 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level20 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level21 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level22 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level23 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level24 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level25 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}
[Serializable]
public class Level26 : LevelCommand
{
    public override void Exit()
    {
        
    }

    public override void Failer()
    {
        LevelManager.Instance.OnLevelFailed(baseValues.meta);
    }

    public override void Setup()
    {
        
    }

    public override void Sucsess()
    {
        LevelManager.Instance.OnLevelSucsess(baseValues.meta);
    }

    public override void Update()
    {
        
    }
}