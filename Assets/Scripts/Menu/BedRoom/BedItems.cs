﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BedItems : MonoBehaviour {

    public Text priceText, energyText, nameText;

    public string iTag;
    public int id;

    void Start()
    {
        ShopInfo info = BedRoom.Instance.shop.FindItem(iTag).FindItem(id);
        priceText.text = info.nutrition.gold.ToString();
        energyText.text = info.nutrition.energy.ToString();
        nameText.text = info.name;
    }

    public void OnClick()
    {
        ShopInfo info = BedRoom.Instance.shop.FindItem(iTag).FindItem(id);
        if (!info.buyed)
        {
            PlayerData.Instance.BuyWithgold(info.nutrition.gold, () =>
            {
                BedRoom.Instance.shop.Buy(iTag, id);
            }, null);
        }
    }
}
