﻿using UnityEngine;
using System.Collections;
using System;

public class Char_Energy : MonoBehaviour {

    public GoldButton goldButton;
    public EnergyInfo[] energys;

    public event Action<float> OnEnergyChanged;

    public float CurEnergy
    {
        get
        {
            float e = 0;
            for (int cnt = 0; cnt < energys.Length; cnt++)
            {
                e += energys[cnt].curEnergy;

            }
            return e;
        }
    }

    public float MaxEnergy
    {
        get
        {
            float e = 0;
            for (int cnt = 0; cnt < energys.Length; cnt++)
            {
                e += energys[cnt].maxEnergy;

            }
            return e;
        }
    }

    public void AddEnergy(string tag, float energy, Action doneMethod, Action failedMethod)
    {
        energys[FindEnergy(tag)].ADD(energy, doneMethod, failedMethod);
        if (OnEnergyChanged != null)
            OnEnergyChanged(CurEnergy);
    }

    public void ReductEnergy( float energy)
    {
        float f = energy / energys.Length;
        for (int cnt = 0; cnt < energys.Length; cnt++)
        {
            energys[cnt].Reduct(f);
        }
        if (OnEnergyChanged != null)
            OnEnergyChanged(CurEnergy);
    }

    public int FindEnergy( string tag )
    {
        return Array.FindIndex(energys, e => e.tag.Contains(tag));
    }

    public string GetInformation()
    {
        string temp = "";
        for (int cnt = 0; cnt < energys.Length; cnt++)
        {
            string s = Statices.Separator(Statices.SEPARATOR_BETWEEN_ITEMSOFLIST, energys[cnt].tag, energys[cnt].curEnergy);
            s = s.TrimEnd(new char[] { Statices.SEPARATOR_BETWEEN_ITEMSOFLIST });
            temp += string.Concat(s, Statices.SEPARATOR_BETWEEN_LISTITEMS);
        }
        temp = temp.TrimEnd(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        return temp;
    }

    public void SetInformation(string txt)
    {
        string[] sp = txt.Split(new char[] { Statices.SEPARATOR_BETWEEN_LISTITEMS });
        for (int cnt = 0; cnt < sp.Length; cnt++)
        {
            string[] sp1 = sp[cnt].Split(new char[] { Statices.SEPARATOR_BETWEEN_ITEMSOFLIST });
            energys[FindEnergy(sp1[0])].Set(sp1[1].ToInt());
        }
    }
}
[Serializable]
public class EnergyInfo
{
    public string tag;
    public float curEnergy;
    public float maxEnergy;
    public void ADD(float e, Action doneMethod, Action failedMethod)
    {
        if (curEnergy < maxEnergy)
        {
            if (doneMethod != null)doneMethod();
        }
        else
        {
            if (failedMethod != null) failedMethod();
        }

        curEnergy = Mathf.Clamp(curEnergy + e, 0, maxEnergy);
    }
    public void Reduct( float e )
    {
        curEnergy = Mathf.Clamp(curEnergy - e, 0, maxEnergy);
    }

    public void Set(float c)
    {
        curEnergy = c;
    }
}
