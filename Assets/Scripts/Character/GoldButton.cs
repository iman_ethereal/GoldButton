﻿using UnityEngine;
using System.Collections;

public class GoldButton : Singleton<GoldButton> {

    public Animator animator;
    public Char_Dialog dialog;
    public Char_Nutrtion nutrition;
    public Char_Energy energy;
    public Char_State state;
    public Char_Customization customize;

    public string GetSaveInformation()
    {
        return Statices.Separator(Statices.SEPARATOR_BETWEEN_GROUPS, energy.GetInformation());
    }

    public void SetLoadInformation(string txt)
    {
        string[] sp = txt.Split(new char[] { Statices.SEPARATOR_BETWEEN_GROUPS });
        energy.SetInformation(sp[0]);
    }
}
