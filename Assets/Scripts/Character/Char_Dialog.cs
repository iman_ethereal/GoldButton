﻿using UnityEngine;
using System.Collections;
using System;

public class Char_Dialog : MonoBehaviour {

    public GoldButton goldButton;
    public AudioClip[] audioClip;
    public AudioSource source;

    public delegate void DelegateDialog();
    public DelegateDialog doneMethod, failedMethod;

    public void PlayRandomClip()
    {
        int i = UnityEngine.Random.Range(0, audioClip.Length);
        BroadcastAudio(i, new DelegateDialog(() => { Debug.Log("done"); }), null);
    }

    public void BroadcastAudio( int index, DelegateDialog doneMethod, DelegateDialog failedMethod)
    {
        StopBroadcasting();
        if (doneMethod != null) this.doneMethod = doneMethod;
        if (failedMethod != null) this.failedMethod = failedMethod;
        _BroadcastEvet(audioClip[index].length);
        source.clip = audioClip[index];
        source.Play();
    }

    IEnumerator _BroadcastEvet(float t)
    {
        yield return new WaitForSeconds(t);
        if (doneMethod != null)
        {
            doneMethod();
            doneMethod = null;
        }
        if (failedMethod != null)
        {
            failedMethod();
            failedMethod = null;
        }
    }

    public void StopBroadcasting()
    {
        CancelInvoke("_BroadcastEvet");
        doneMethod = null;
        failedMethod = null;
        source.Stop();
        source.clip = null;
    }
}
