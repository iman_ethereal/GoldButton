﻿using UnityEngine;
using System.Collections;
using System;

public class Char_Customization : MonoBehaviour {

    public CustomizeCharBaseInfo[] infos;
    public CustomizeCharBaseInfo[] defultInfos;
    public GameObject hair;

    private Shop shop;

    void Start()
    {
        shop = DressRoom.Instance.shop;
        EquipAll();
    }

    public void Equip(string classMode, int id)
    {
        FindItem(classMode).DesarmamentAll();
        DesarmamentDef(classMode);
        FindItem(classMode).Equip(id);
    }

    public void Desarmament(string classMode, int id)
    {
        FindItem(classMode).Desarmament(id);
        FindItem("Default").Equip(0);
    }

    public CustomizeCharBaseInfo FindItem( string classMode)
    {
        return Array.Find(infos, c => c.classMode.Contains(classMode));
    }

    public CustomizeCharBaseInfo FindDefaultes(string classMode)
    {
        return Array.Find(defultInfos, c => c.classMode.Contains(classMode));
    }
    public bool ContainsDefault(string tag)
    {
        return Array.Find(defultInfos, c => c.classMode.Contains(tag)) != null;
    }

    public CustomizeCharItemInfo FindItem(string classMode, int id)
    {
        return FindItem(classMode).FindItem(id);
    }

    public void EquipAll()
    {
        DesarmamentAll();
        for (int cnt = 0; cnt < shop.infos.Length; cnt++)
        {
            for (int x = 0; x < shop.infos[cnt].items.Length; x++)
            {
                if( shop.infos[cnt].items[x].select)
                {
                    DesarmamentDef(shop.infos[cnt].tag);
                    FindItem(shop.infos[cnt].tag, shop.infos[cnt].items[x].id).Equip(); 
                }
                if (!shop.infos[cnt].HasSelectedItem())
                    if (ContainsDefault(shop.infos[cnt].tag))
                    {
                        DesarmamentDef(shop.infos[cnt].tag);
                        FindDefaultes(shop.infos[cnt].tag).Equip(0);
                    }
            }
        }
        SetHairColor();
    }

    public void SetHairColor()
    {
        if (DressRoom.Instance.shop.FindItem("HairColor").HasSelectedItem())
        {
            ShopInfo s = DressRoom.Instance.shop.FindItem("HairColor").GetSelectedItem();
            Color c = FindItem("HairColor", s.id).color;
            hair.GetComponent<UnityEngine.UI.Image>().color = c;
        }
        else
        {
            hair.GetComponent<UnityEngine.UI.Image>().color = Color.white;
        }
    }

    public void DesarmamentDef(string tag)
    {
        if( ContainsDefault(tag) )
            FindDefaultes(tag).DesarmamentAll();
    }

    private void DesarmamentAll()
    {
        for (int cnt = 0; cnt < infos.Length; cnt++)
        {
            infos[cnt].DesarmamentAll();
        }
        for (int cnt = 0; cnt < defultInfos.Length; cnt++)
        {
            defultInfos[cnt].DesarmamentAll();
        }
    }
}
[Serializable]
public class CustomizeCharBaseInfo
{
    public string classMode;
    public CustomizeCharItemInfo[] items;

    public void Equip(int id)
    {
        FindItem(id).Equip();
    }

    public void Desarmament(int id)
    {
        FindItem(id).Disarmament();
    }

    public void DesarmamentAll()
    {
        foreach (CustomizeCharItemInfo item in items)
        {
            if (item.equip)
                item.Disarmament();
        }
    }

    public CustomizeCharItemInfo FindItem(int id)
    {
        return Array.Find(items, c => c.id == id);
    }
}
[Serializable]
public class CustomizeCharItemInfo
{
    public int id;
    public bool equip
    {
        get
        {
            if (items.Length > 0)
                return items[0].activeSelf;
            else
                return false;
        }
    }
    public GameObject[] items;
    public Color color;

    public void Equip()
    {
        foreach (GameObject item in items)
        {
            item.SetActive(true);
        }
    }
    public void Disarmament()
    {
        foreach (GameObject item in items)
        {
            item.SetActive(false);
        }
    }
}

