﻿using UnityEngine;
using System.Collections;
using System;

public class Char_State : MonoBehaviour {

    public GoldButton goldButton;

    public StateInfo[] states;

    void Start()
    {
        GoToState("Idle");
    }

    public StateInfo FindStateWithState(string state)
    {
        return Array.Find(states, s => s.State.Contains(state));
    }
    public StateInfo FindStateWithMecanim(string mecanim)
    {
        return Array.Find(states, s => s.MecanimName.Contains(mecanim));
    }

    public void GoToState(string state)
    {
        goldButton.animator.CrossFadeInFixedTime(FindStateWithState(state).MecanimName, 0);
    }

    public string FindStat()
    {
        string temp = "";
        for (int cnt = 0; cnt < states.Length; cnt++)
        {
            if (goldButton.animator.GetCurrentAnimatorStateInfo(0).IsName(states[cnt].MecanimName))
            {
                temp = states[cnt].MecanimName;
            }
        }
        return temp;
    }
}
[Serializable]
public class StateInfo
{
    public string State;
    public string MecanimName;
}
