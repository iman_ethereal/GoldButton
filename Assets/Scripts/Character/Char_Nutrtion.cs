﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Char_Nutrtion : MonoBehaviour {

    public GoldButton goldbutton;
    public ButtonTrigger trigger;
    public UsedClasses useClasses;

    public delegate void DelegateNutrition(NutritionType type);
    public event DelegateNutrition OnUseNutrition;

    void Start()
    {
        trigger.OnDropEvent = OnDropedOnMe;
        useClasses = new UsedClasses(goldbutton);
    }

    private void OnDropedOnMe(ButtonTrigger bt)
    {
        SendShopInfoObject obj = (SendShopInfoObject)DragImageScript.Instance.carriedObject;
        if( obj != null )
            UseNutrition(obj);
    }

    public void UseNutrition(SendShopInfoObject info)
    {
        Debug.Log(info.tag);
        switch (info.info.nutrition.type)
        {
            case NutritionType.Eat:
                useClasses.Used_Eat.Activate(info);
                break;
            case NutritionType.PlayAndRead:
                useClasses.Used_ReadBook.Activate(info);
                break;
            case NutritionType.Clothing:
                useClasses.Used_Clothing.Activate(info);
                break;
            case NutritionType.Sleep:
                useClasses.Used_Sleep.Activate(info);
                break;
        }
        if (OnUseNutrition != null)
            OnUseNutrition(info.info.nutrition.type);
    }
}
[Serializable]
public class Nutrition
{
    public int gold;
    public int energy;
    public NutritionType type; 
}

public enum NutritionType
{
    Eat, PlayAndRead, Clothing, Sleep
}

[Serializable]
public class UsedClasses
{
    public UsedClasses(GoldButton gold)
    {
        Used_Eat = new UsedSucsess_Eat(gold);
        Used_Clothing = new UsedSucsess_Clothing(gold);
        Used_ReadBook = new UsedSucsess_PlayAndRead(gold);
        Used_Sleep = new UsedSucsess_Sleep(gold);
    }
    public UsedSucsess_Eat Used_Eat;
    public UsedSucsess_Clothing Used_Clothing;
    public UsedSucsess_PlayAndRead Used_ReadBook;
    public UsedSucsess_Sleep Used_Sleep;
}

[Serializable]
public class UsedSucsess_Eat : UsedCommand
{
    public UsedSucsess_Eat(GoldButton gold):base(gold)
    {
    }

    public override void Activate(SendShopInfoObject info)
    {
        GoldButton.energy.AddEnergy(info.info.nutrition.type.ToString(), info.info.nutrition.energy, ()=> 
        {
            FreezerShop.Instance.shop.Use(info.tag, info.info.id);
            Debug.Log("Eated");
            GoldButton.state.GoToState("Eating");
        }, ()=> 
        {
            Debug.Log("Not Eated");
            GoldButton.state.GoToState("Refuse");

        });
    }
}

[Serializable]
public class UsedSucsess_Clothing : UsedCommand
{
    public UsedSucsess_Clothing(GoldButton gold):base(gold)
    {
    }
    public override void Activate(SendShopInfoObject info)
    {
        GoldButton.energy.AddEnergy(info.info.nutrition.type.ToString(), info.info.nutrition.energy, () =>
        {
            switch (info.tag)
            {
                case "Lenz":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup"); break;
                case "Eyekash":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "Tel":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "Glasses":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "HairColor":
                    Debug.Log("asdads");
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.SetHairColor();
                    Debug.Log("Suiteup");
                    break;
                case "Sticker":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "Hat":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
            }

        }, () =>
        {
            switch (info.tag)
            {
                case "Lenz":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup"); break;
                case "Eyekash":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "Tel":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "Glasses":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "HairColor":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.SetHairColor();
                    Debug.Log("Suiteup");
                    break;
                case "Sticker":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
                case "Hat":
                    DressRoom.Instance.shop.Select(info.tag, info.info.id);
                    GoldButton.customize.Equip(info.tag, info.info.id);
                    Debug.Log("Suiteup");
                    break;
            }
        });
    }
}

[Serializable]
public class UsedSucsess_PlayAndRead : UsedCommand
{
    public UsedSucsess_PlayAndRead(GoldButton gold) : base(gold)
    {

    }
    public override void Activate(SendShopInfoObject info)
    {
        HappyShop.Instance.SetBook(info.info.id);
        GoldButton.state.GoToState("Reading");
    }
}

[Serializable]
public class UsedSucsess_Sleep : UsedCommand
{
    public UsedSucsess_Sleep(GoldButton gold) : base(gold)
    {

    }
    public override void Activate(SendShopInfoObject info)
    {
        GoldButton.energy.AddEnergy(info.info.nutrition.type.ToString(), info.info.nutrition.energy, () =>
        {
            Debug.Log("Sleeped");
        }, () =>
        {
            Debug.Log("Not Sleeped");
        });
    }
}